<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function welcome(Request $request) {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('pages.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang ]);
    }
}
