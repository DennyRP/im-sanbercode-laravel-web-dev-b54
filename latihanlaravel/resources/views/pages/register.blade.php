<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <form action="/welcome" method="post">
        @csrf

        <label>First name:</label> <br> <br>
        <input type="text" name="fname"> <br> <br>
        
        <label>Last name:</label> <br> <br>
        <input type="text" name="lname"> <br> <br>

        <label>Gender:</label> <br> <br>
        <input type="radio" name="Gender" value="1"> Male <br>
        <input type="radio" name="Gender" value="2"> Female <br>
        <input type="radio" name="Gender" value="3"> Other <br> <br>

        <label>Nationality:</label> <br> <br>
        <select name="Nationality">
            <option value="1">Indonesia</option>
            <option value="2">Palestina</option>
            <option value="3">Korea</option>
        </select> <br> <br>

        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="lang" value="1"> Bahasa Indonesia <br>
        <input type="checkbox" name="lang" value="2"> English <br> 
        <input type="checkbox" name="lang" value="3"> Other <br> <br>

        <label>Bio:</label> <br> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">


    </form>
</body>
</html>